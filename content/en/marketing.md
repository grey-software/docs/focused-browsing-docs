---
title: Marketing Materials
description: Check out the marketing materials for Grey Software's Focused Browsing project.
position: 100
category: Marketing
---

## Source Files 

We used Figma to create our marketing materials. 

<cta-button text="Open Figma Page" link="https://www.figma.com/file/2gSU3IRPFTSquJg8RrRhMK/?node-id=537%3A787"></cta-button>

## Chrome Web Store

### Marquee Promo

![Focused Browsing Promo](/promo-marquee.png)

### Large Promo

![Focused Browsing Promo](/promo-large.png)

### Small Promo

![Focused Browsing Promo](/promo-small.png)

### Screenshots

![Screenshot showcasing hiding distractions without leaving the tab you're on](/screenshot-1.png)

![Screenshot showcasing controlling focus using keyboard shortcuts](/screenshot-2.png)

![Screenshot showcasing Dim and Dark mode support](/screenshot-3.png)



