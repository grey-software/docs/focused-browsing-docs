---
title: Pitch Deck
description: Check out the pitch for Grey Software's Focused Browsing project.
position: 101
category: Marketing
---

## The Problem

![The Problem](pitch/1.png)

## The Solution

![The Solution](pitch/2.png)

## Introducing Focused Browsing

![Introducing Focused Browsing](pitch/3.png)

Focused Browsing allows you to show and hide distracting feeds designed to grab your attention and waste your time while browsing the web.

Our browser extension gives you the option to go into to focus mode, where elements like the news feed, trending hashtags, and others are hidden.

By pressing a simple keyboard shortcut, you can choose when you want to stay focused on your creative and professional endeavors.

## The Market

![The Market](pitch/4.png)

News feed eradicator has over [200,000 users on Chrome](https://chrome.google.com/webstore/detail/news-feed-eradicator/fjcldmjmjhkklehbacihaiopjklihlgg?hl=en) and over [6000 users on Firefox](https://addons.mozilla.org/en-US/firefox/addon/news-feed-eradicator/).

Hide Twitter ads has over [2000 users on chrome](https://chrome.google.com/webstore/detail/hide-twitter-ads-block-pr/bapmhjebfdbdpjjfafnkfidijkjlkakf)

Insta feed eradicator has over [2000 users on chrome](https://chrome.google.com/webstore/detail/insta-feed-eradicator/opmlbbpajagbjblnnhjhiedihdnlochl?hl=en)

Noah Kagan(founder of AppSumo) mentioned both of these extensions in [his video listing the 15 best chrome extensions he recommends](https://www.youtube.com/watch?v=sP1aQtZiiZE). Noah has a channel of over 179,000 subscribers, and he makes content that appeals to entrepreneurs starting businesses.

Based on the number of installs on extensions whose functionality we would be replicating, the market is over 200,000 people at a minimum.

If we consider the [50 million social website users that only use their desktops](https://backlinko.com/social-media-users), and our offer is valuable to 0.02% of them, **the potential market to capture is at least 1 million.**

## Our Unique Insight

![Our Unique Insight](pitch/5.png)

- Our extension offers users to toggle between focused and unfocused modes without leaving their tab using a simple keyboard shortcut. **The other extensions on the market do not support this!**
- We are building a strong community of open source collaborators by creating an educational ecosystem of documentation and code labs about developing focused browsing. Through this, we hope to gain more insights into ways to expanding the product's functionality and potentially extend it to serve people on mobile-native social applications.

## Our Business Model

![Our Business Model](pitch/6.png)

Focused Browsing will generate revenue by offering exclusive features to [Grey software's app collection](https://hackmd.io/@grey-software/H1796hy9_) sponsors!

## Our Progress

![Our Progress](pitch/7.png)

- We have published our product on the [Chrome](https://chrome.google.com/webstore/detail/ocbkghddheomencfpdiblibbjhjcojna) and [Firefox](https://addons.mozilla.org/en-US/firefox/addon/focused-browsing/) extension marketplaces.
- We are currently working on a beta release which we will announce on Hackernews, ProductHunt, subreddits, and others.

### What's Next?

![What's Next](pitch/8.png)

### What we want?

![What's Next](pitch/9.png)

### Our Team

![Our Team](pitch/10.png)
