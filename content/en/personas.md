---
title: Personas
description: Who are the personas we're serving with Focused Browsing?
position: 2
category: Overview
---

## Arsala the busy remote tech professional

Arsala is a busy remote tech professional who mainly uses LinkedIn and Twitter to network with individuals for business and mentorship. He values his focused time and feels strained when he is bombarded with attention-grabbing content during this time. Arsala wishes there was a way to regain sovereignty of his attention when visiting websites essential to his work. 

Arsala currently uses News Feed Eradicator, but he does not trust that the individual OSS maintainer will indefinitely support the project. He wishes a stable company maintained an open-source solution so he can trust that updates will regularly be issued. He also hopes that the company maintainers can hear his requests regarding the future of the solution on open forums and add additional websites to the application by forking his local version or adding to the global open source solution.